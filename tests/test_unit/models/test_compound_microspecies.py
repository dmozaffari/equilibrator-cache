# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Verify the CompoundMicrospecies class behavior."""


import hypothesis.strategies as st
import pytest
from hypothesis import example, given

from equilibrator_cache.models.compound_microspecies import CompoundMicrospecies


@given(
    id=st.integers(),
    compound_id=st.integers(),
    charge=st.integers(),
    number_protons=st.integers(),
    is_major=st.booleans(),
    ddg_over_rt=st.floats(),
)
@example(
    id=None,
    compound_id=None,
    charge=None,
    number_protons=None,
    is_major=None,
    ddg_over_rt=None,
)
def test___init__(**kwargs):
    """Ensure a consistent string representation."""
    CompoundMicrospecies(**kwargs)


@pytest.mark.parametrize(
    "cmpd_ms, expected",
    [
        (
            CompoundMicrospecies(),
            "CompoundMicrospecies(compound_id=None, charge=None, "
            "number_protons=None)",
        ),
        (
            CompoundMicrospecies(compound_id=1),
            "CompoundMicrospecies(compound_id=1, charge=None, "
            "number_protons=None)",
        ),
        (
            CompoundMicrospecies(charge=2),
            "CompoundMicrospecies(compound_id=None, charge=2, "
            "number_protons=None)",
        ),
        (
            CompoundMicrospecies(number_protons=3),
            "CompoundMicrospecies(compound_id=None, charge=None, "
            "number_protons=3)",
        ),
    ],
)
def test___repr__(cmpd_ms: CompoundMicrospecies, expected: str):
    """Ensure a consistent string representation."""
    assert repr(cmpd_ms) == expected


@pytest.mark.parametrize("cmpd_ms, energy", [])
def test_transform(cmpd_ms: CompoundMicrospecies, energy: float):
    """Verify expected free energy transformation."""
    # TODO (Moritz): Add some known test cases.
    assert False
