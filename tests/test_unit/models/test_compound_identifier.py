# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Verify the CompoundIdentifier class behavior."""


import pytest

import equilibrator_cache.models.compound_identifier as cmpd_id


@pytest.mark.parametrize(
    "kwargs", [{"compound_id": 1, "registry_id": 1, "accession": "h2o"}]
)
def test___init__(kwargs):
    """Ensure initialization occurs as expected."""
    identifier = cmpd_id.CompoundIdentifier(**kwargs)
    for key, value in kwargs.items():
        assert getattr(identifier, key) == value


@pytest.mark.parametrize(
    "kwargs, namespace, expected",
    [
        (
            {"compound_id": 1, "registry_id": 1, "accession": "h2o"},
            "metanetx.compound",
            "CompoundIdentifier(registry=foo, accession=h2o)",
        )
    ],
)
def test___repr__(mocker, kwargs, namespace, expected):
    """Ensure a consistent string representation."""
    identifier = cmpd_id.CompoundIdentifier(**kwargs)
    mock = mocker.MagicMock()
    mock.__repr__ = mocker.Mock(return_value="foo")
    identifier.registry = mock
    assert repr(identifier) == expected
    assert mock.__repr__.call_count == 1


@pytest.mark.parametrize(
    "kwargs, expected",
    [
        ({"compound_id": 1, "registry_id": 1, "accession": "h2o"}, True),
        ({"compound_id": 1, "registry_id": 1, "accession": "h2o"}, False),
    ],
)
def test_is_valid(mocker, kwargs, expected):
    """Verify expected validation behavior."""
    identifier = cmpd_id.CompoundIdentifier(**kwargs)
    mock = mocker.MagicMock()
    mock.is_valid_accession.return_value = expected
    identifier.registry = mock
    assert identifier.is_valid() == expected
    mock.is_valid_accession.assert_called_once_with(kwargs["accession"])
