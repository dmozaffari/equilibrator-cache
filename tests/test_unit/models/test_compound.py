# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Verify the Compound class behavior."""


import hypothesis.strategies as st
import pytest
from hypothesis import example, given

import equilibrator_cache.models.compound as cmpd


@given(id=st.integers(), inchi_key=st.text(), inchi=st.text(), smiles=st.text())
@example(id=None, inchi_key=None, inchi=None, smiles=None)
def test___init__(**kwargs):
    """Ensure a consistent string representation."""
    cmpd.Compound(**kwargs)


@pytest.mark.parametrize(
    "compound, expected",
    [
        (cmpd.Compound(), "Compound(id=None, inchi_key=None)"),
        (cmpd.Compound(id=1), "Compound(id=1, inchi_key=None)"),
        (
            cmpd.Compound(inchi_key="sdvwvw"),
            "Compound(id=None, inchi_key=sdvwvw)",
        ),
        (
            cmpd.Compound(id=2, inchi_key="sdvwvw"),
            "Compound(id=2, inchi_key=sdvwvw)",
        ),
    ],
)
def test___repr__(compound: cmpd.Compound, expected: str):
    """Ensure a consistent string representation."""
    assert repr(compound) == expected


@pytest.mark.parametrize(
    "compound, formula",
    [
        (cmpd.Compound(atom_bag={}), ""),
        (cmpd.Compound(atom_bag={"C": 1}), "C"),
        (cmpd.Compound(atom_bag={"C": 1, "H": 0}), "C"),
        (cmpd.Compound(atom_bag={"C": 1, "H": 0, "O": 2}), "CO2"),
        (cmpd.Compound(atom_bag={"e-": 4, "C": 1, "O": 2}), "CO2"),
        (cmpd.Compound(atom_bag={"C": 101, "O": 22}), "C101O22"),
    ],
)
def test_formula(compound: cmpd.Compound, formula: str):
    """Verify expected formula composition."""
    assert compound.formula == formula


@pytest.mark.parametrize("compound, energy", [])
def test_transform(compound: cmpd.Compound, energy: float):
    """Verify expected free energy transformation."""
    # TODO (Moritz): Add some known test cases.
    assert False
